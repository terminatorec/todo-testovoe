# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).



## Ссылка на проект загруженый на хостинг vercel

https://todo-testovoe-eight.vercel.app/ \
(Чтобы начать пользоваться, вам нужно залогиниться через гугл)\

## Ссылка на проект загруженый на gitlab

https://gitlab.com/terminatorec/todo-testovoe.git \
в папке out находится документация JSDOC, я прокоментировал функции приложения\

## Некоторая информация про проект

Бэкенд - авторизация, базы данных, сохранение файлов, все сделано на firebase\

В проекте использовал дополнительные библиотеки:\
• react-firebase-hooks - для удобства работы с firebase\
• react-icons - мне нужны были иконки\
• react-datetime-picker - использовал библиотеку с компонентом выбора даты и времени\


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.


